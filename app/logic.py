import json


def getBahanBaku():
    with open('static/data/makanan.json', 'r') as f:
        data = json.load(f)
    return data['bahanBaku']


def getKategori():
    with open('static/data/makanan.json', 'r') as f:
        data = json.load(f)
    return data['kategori']


def get_all_item():
    return [item for item in getBahanBaku().keys()]


def get_all_food():
    allFood = []
    for bahanBaku in getBahanBaku():
        is_made_by = Makan(bahanBaku)
        for food in is_made_by:
            allFood.append(food)
    return set(allFood + get_all_item())


def TerbuatDari(x):
    return [y for y in getBahanBaku()[x]['menu']]


def OlahanDari(x):
    return [y for y in getBahanBaku()[x]['olahan']]


def Makan(x):
    return TerbuatDari(x) + OlahanDari(x)


def Restriction(daftarKategori):
    Mengandung = []
    for z in daftarKategori:
        if z == "kolesterol":
            if daftarKategori[z] == True:
                Mengandung += getKategori()["kolesterol"]
        if z == "pedas":
            if daftarKategori[z] == True:
                Mengandung += getKategori()["pedas"]
        if z == "non-vegetarian":
            if daftarKategori[z] == True:
                Mengandung += [food for food in get_all_food() if food not in getKategori()['vegetarian']]
        if z == "haram":
            if daftarKategori[z] == True:
                Mengandung += getKategori()["haram"]
    return Mengandung


def BisaMakan(available, restrictions):
    result = available
    for food_restriction in restrictions:
        if food_restriction in result:
            result.remove(food_restriction)
    return result


def InginMakant(x):
    inputBahan = x
    result = []
    for item in inputBahan:
        is_made_by = Makan(item)
        #result = result + Makan(item)
        for food in is_made_by:
            if food in getBahanBaku():
                inputBahan.append(food)
        result = result + is_made_by
    return result


def InginMakan(bahan):
    # 5. Bahan(x) -> InginMakan(x)
    inputBahan = bahan
    inginMakan = inputBahan
    Makan = []
    for x in inginMakan:
        if x == "Ayam":
            # 2. OlahanDari(y, x)
            # 3. TerbuatDari(y, x)
            # 6. InginMakan(x) ^ OlahanDari(y, x) -> MakanOlahan(y) P->Q
            # 7. InginMakan(x) ^ TerbuatDari(y, x) -> MakanMenu(y) R->S
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Ayam")
            makanOlahan = OlahanDari("Ayam")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            # 9. MakanOlahan(y) v MakanMenu(z) -> Makan([y,z])
            Makan = Makan + makanMenu + makanOlahan
        if x == "Udang":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Udang")
            makanOlahan = OlahanDari("Udang")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Susu":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Susu")
            makanOlahan = OlahanDari("Susu")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Tepung-terigu":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Tepung-terigu")
            makanOlahan = OlahanDari("Tepung-terigu")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Tepung-tapioka":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Tepung-tapioka")
            makanOlahan = OlahanDari("Tepung-tapioka")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Sapi":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Sapi")
            makanOlahan = OlahanDari("Sapi")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Kelapa":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Kelapa")
            makanOlahan = OlahanDari("Kelapa")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Kambing":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Kambing")
            makanOlahan = OlahanDari("Kambing")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Kedelai":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Kedelai")
            makanOlahan = OlahanDari("Kedelai")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Madu":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Madu")
            makanOlahan = OlahanDari("Madu")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Singkong":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Singkong")
            makanOlahan = OlahanDari("Singkong")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Ikan":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Ikan")
            makanOlahan = OlahanDari("Ikan")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Cokelat":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Cokelat")
            makanOlahan = OlahanDari("Cokelat")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Babi":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Babi")
            makanOlahan = OlahanDari("Babi")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Telur":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Telur")
            makanOlahan = OlahanDari("Telur")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Kacang-tanah":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Kacang-tanah")
            makanOlahan = OlahanDari("Kacang-tanah")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Keju":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Keju")
            makanOlahan = OlahanDari("Keju")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Kentang":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Kentang")
            makanOlahan = OlahanDari("Kentang")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Cumi":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Cumi")
            makanOlahan = OlahanDari("Cumi")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        if x == "Jagung":
            Makan.append(x.lower())
            makanMenu = TerbuatDari("Jagung")
            makanOlahan = OlahanDari("Jagung")
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
        else:
            Makan.append(x.lower())
            makanMenu = TerbuatDari(x)
            makanOlahan = OlahanDari(x)
            for food in makanMenu:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            for food in makanOlahan:
                if food.capitalize() in getBahanBaku():
                    inginMakan.append(food.capitalize())
            Makan = Makan + makanMenu + makanOlahan
    return list(set(Makan)) 


def cant_eatt(x):
    inputBahan = x
    inginMakan = inputBahan
    result = []
    is_made_by = []
    for item in inginMakan:
        is_made_by = is_made_by + Makan(item)
    for food in get_all_food():
        if food not in is_made_by:
            result.append(food)
    return result


def TidakInginMakan(bahan):
    # 1. Bahan(x)
    inputBahan = bahan
    tidakInginMakan = inputBahan
    Makan = []
    TidakMakan = []
    for x in tidakInginMakan:
        if x == "Ayam":
            # 2. OlahanDari(y,x)
            # 3. TerbuatDari(y,x)
            # 6. TidakInginMakan(x) ^ OlahanDari(y, x) -> TidakMakanOlahan(y) P->Q
            # 7. TidakInginMakan(x) ^ TerbuatDari(y, x) -> TidakMakanMenu(y) R->S
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            # 9. TidakMakanOlahan(y) v TidakMakanMenu(z) -> TidakMakan(y)
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Udang":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Susu":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Tepung-terigu":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Tepung-tapioka":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Sapi":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kelapa":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kambing":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kedelai":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Madu":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Singkong":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Ikan":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Cokelat":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Babi":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Telur":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kacang-tanah":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Keju":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kentang":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Cumi":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Jagung":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        else:
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
    # 10. Ɐy Ɐz TidakMakan(z) -> Makan(y), y ⊄ z
    if TidakMakan:
        for y in get_all_food():
            if y not in TidakMakan:
                Makan.append(y)
        return list(set(Makan))

def food_icant_eat(bahan):
    # 1. Bahan(x)
    inputBahan = bahan
    tidakInginMakan = inputBahan
    TidakMakan = []
    for x in tidakInginMakan:
        if x == "Ayam":
            # 2. OlahanDari(y,x)
            # 3. TerbuatDari(y,x)
            # 6. TidakInginMakan(x) ^ OlahanDari(y, x) -> TidakMakanOlahan(y) P->Q
            # 7. TidakInginMakan(x) ^ TerbuatDari(y, x) -> TidakMakanMenu(y) R->S
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            # 9. TidakMakanOlahan(y) v TidakMakanMenu(z) -> TidakMakan(y)
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Udang":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Susu":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Tepung-terigu":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Tepung-tapioka":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Sapi":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kelapa":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kambing":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kedelai":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Madu":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Singkong":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Ikan":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Cokelat":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Babi":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Telur":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kacang-tanah":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Keju":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Kentang":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Cumi":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        if x == "Jagung":
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
        else:
            TidakMakan.append(x)
            TidakMakanMenu = TerbuatDari(x)
            TidakMakanOlahan = OlahanDari(x)
            for food in TidakMakanMenu:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            for food in TidakMakanOlahan:
                if food.capitalize() in getBahanBaku():
                    tidakInginMakan.append(food.capitalize())
            TidakMakan = TidakMakan + TidakMakanMenu + TidakMakanOlahan
    return list(set(TidakMakan))

def both(x, y):
    food_want = InginMakan(x)
    food_dont = food_icant_eat(y)
    Makan = []
    for item in food_want:
        if item not in food_dont:
            Makan.append(item)
    return Makan
