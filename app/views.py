from django.shortcuts import render
from .logic import *
from django.http import HttpResponseRedirect
import json
response = {}
# Create your views here.


def homepage(request):
    return render(request, "home.html")


def inputPage_foodCantBeEaten(request):
    menu = get_all_item()
    response = {'menu': menu}
    return render(request, "inputPage_FoodICantEat.html", response)


def inputPage_foodWantBeEaten(request):
    menu = get_all_item()
    response = {'menu': menu}
    return render(request, "inputPage_IWantEat.html", response)


def inputPage_foodBothOfThem(request):
    menu = get_all_item()
    response = {'menu': menu}
    return render(request, "inputPage_BothOfThem.html", response)


def resultPage_foodICanEat(request):
    print(response)
    return render(request, "ResultPage_ICanEat.html", response)


def resultPage_foodICantEat(request):
    return render(request, "ResultPage_ICantEat.html", response)


def resultPage_both(request):
    return render(request, "ResultPage_both.html", response)


def compute_ICanEat(request):
    if (request.method == 'POST'):
        print(request.POST['restrictions'])
        restrictions = json.loads(request.POST['restrictions'])
        response['food'] = []
        bahan = request.POST.getlist('foods[]')
        if bahan: foods_available = InginMakan(bahan)
        foods_restrictions = Restriction(restrictions)
        foods_result = BisaMakan(foods_available, foods_restrictions)
        response['food'] = foods_result
    return HttpResponseRedirect('/result/food-i-can-eat')


def compute_ICantEat(request):
    if (request.method == 'POST'):
        print(request.POST['restrictions'])
        restrictions = json.loads(request.POST['restrictions'])
        response['food'] = []
        bahan = request.POST.getlist('foods[]')
        if bahan: foods_allowed = TidakInginMakan(bahan)
        foods_restrictions = Restriction(restrictions)
        foods_result = BisaMakan(foods_allowed, foods_restrictions)
        response['food'] = foods_result
    return HttpResponseRedirect('/result/food-i-cant-eat')


def compute_both(request):
    if (request.method == "POST"):
        foods_wanted = request.POST.getlist('foods_want[]')
        foods_not_wanted = request.POST.getlist('foods_dont[]')
        restrictions = json.loads(request.POST['restrictions'])
        # clear previous response data
        response['food'] = []
        foods_restrictions = Restriction(restrictions)
        # ini lho gua gak tauuu inferencenya gmn ded
        foods_allowed = both(foods_wanted, foods_not_wanted)
        foods_result = BisaMakan(foods_allowed, foods_restrictions) 
        response['food'] = foods_result
    return HttpResponseRedirect('/result/both-of-them/')
