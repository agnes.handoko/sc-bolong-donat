# MakananQ

Sebuah web application yang dapat membantu user dalam menentukan makanan apa saja yang bisa dimakan dengan tiga pilihan jenis input: Food i want to eat, Food i don’t want to eat, Both of it

## Developer:
1. Agnes Handoko - 1706039761
2. Chrysant Celine S. - 1706039862
3. Fakhira Devina - 1706979221
4. Karina Ivana - 1706979335

## Links:
*  Heroku: eatornoteat.herokuapp.com
*  Figma: https://www.figma.com/file/sgim9pc82lm3RUsJ8ngHlw/Mock-Up-MakananQ?node-id=10%3A3
*  Video: https://www.youtube.com/watch?v=bUGrNQt5UE0
*  PPT: http://bit.ly/MakananQ
*  Gitlab: https://gitlab.com/agnes.handoko/sc-bolong-donat

## Penjelasan Aplikasi
Input	: Bahan Makanan
Output	: Olahan Makanan

**BAHAN** 
1. Ayam
2. Udang
3. Susu
4. Tepung terigu
5. Tepung tapioka / Maizena
6. Sapi
7. Kelapa
8. Kambing
9. Kedelai
10. Madu
11. Ikan
12. Cokelat
13. Babi
14. Telor
15. Kacang tanah
16. Keju
17. Kentang
18. Cumi
19. Jagung
20. Singkong

## Food I want to eat
1. Bahan(x) 
2. OlahanDari(y, x)
3. TerbuatDari(y, x)
4. Restriction(z)
5. Bahan(x) -> InginMakan(x)
6. InginMakan(x) ^ OlahanDari(y, x) -> MakanOlahan(y) P->Q
7. InginMakan(x) ^ TerbuatDari(y, x) -> MakanMenu(y) R->S
8. InginMakan(x) -> [InginMakan(x) ^ OlahanDari(y, x)] v [InginMakan(x) ^ TerbuatDari(y, x)]
9. MakanOlahan(y) v MakanMenu(z) -> Makan([y,z])
10. Restriction(z) ^ Makan([y,x]) -> BisaMakan(Makan([y,x]), ~(Makan(Mengandung(z))))

## Food I dont want to eat
1. Bahan(x)
2. OlahanDari(y, x)
3. TerbuatDari(y, x)
4. Restriction(z)
5. Bahan(x) -> TidakInginMakan(x)
6. TidakInginMakan(x) ^ OlahanDari(y, x) -> TidakMakanOlahan(y) P->Q
7. TidakInginMakan(x) ^ TerbuatDari(y, x) -> TidakMakanMenu(y) R->S
8. TidakInginMakan(x) -> [TidakInginMakan(x) ^ OlahanDari(y, x)] v [TidakInginMakan(x) ^ TerbuatDari(y, x)]
9. TidakMakanOlahan(y) v TidakMakanMenu(z) -> TidakMakan(y)
10. Ɐy Ɐz TidakMakan(z) -> Makan(y), y ⊄ z
11. Restriction(z)  ^ Makan(y) -> BisaMakan(Makan(y), ~(Makan(Mengandung(z))))