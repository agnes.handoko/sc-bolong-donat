var menu_want_selected = []
var menu_dont_selected = []
$(".dropdown-item").click(function () {
    var id = $(this).attr('id');
    var isWant = id.substring(0, 9) == "menu-want";
    var value = $("#" + id).text().replace(/-/g, " ");
    if (menu_want_selected.indexOf($("#" + id).text()) == -1 && menu_dont_selected.indexOf($("#" + id).text()) == -1) {
        if (isWant) {
            $("#selected-menu").append("<div class=\"d-flex justify-content-center align-items-center flex-column selected-apa\"><article>" + value + "</article></div>");
            menu_want_selected.push(value.replace(/ /g, "-"));
            console.log("menu-want ${menu_want_selected}");
        } else {
            $("#selected-menu").append("<div class=\"d-flex justify-content-center align-items-center flex-column selected-apa-not\"><article>" + value + "</article></div>");
            menu_dont_selected.push(value.replace(/ /g, "-"));
            console.log("menu-want ${menu_dont_selected}");
        };
    }
    console.log(menu_want_selected)
    console.log(menu_dont_selected)
})
var restriction = {
    'kolesterol': false,
    'pedas': false,
    'non-vegetarian': false,
    'haram': false
}

$("#buttonfind").click(function () {
    $.each(restriction, function (key, val) {
        if ($("#" + key).is(":checked")) {
            restriction[key] = true;
        }
    })
    $.ajax({
        type: "POST",
        dataType: 'html',
        headers: {
            'X-CSRFToken': $('input[name=csrfmiddlewaretoken]').val()
        },
        url: '/compute/both/',
        data: {
            restrictions: JSON.stringify(restriction),
            foods_want: menu_want_selected,
            foods_dont: menu_dont_selected,
        },
        success: function (result) {
            window.location = "/result/both-of-them/"
        }
    });
})