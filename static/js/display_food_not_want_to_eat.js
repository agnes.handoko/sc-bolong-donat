function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}

var menu_selected = []
$(".dropdown-item").click(function () {
  var id = $(this).attr('id');
  var value = $("#" + id).text().replace(/-/g, " ");
  if (menu_selected.indexOf($("#" + id).text()) == -1) {
    $("#selected-menu").append("<div class=\"d-flex justify-content-center align-items-center flex-column selected-apa\"><article>" + value + "</article></div>");
    menu_selected.push(value.replace(/ /g, "-"));
  }
  console.log(menu_selected)
})
var restriction = {
  'kolesterol': false,
  'pedas': false,
  'non-vegetarian': false,
  'haram': false
}

$("#buttonfind").click(function () {
  $.each(restriction, function (key, val) {
    if ($("#" + key).is(":checked")) {
      restriction[key] = true;
    }
  })
  $.ajax({
    type: "POST",
    dataType: 'html',
    headers: {
      'X-CSRFToken': $('input[name=csrfmiddlewaretoken]').val()
    },
    url: '/compute/food-i-cant-eat/',
    data: {
      restrictions: JSON.stringify(restriction),
      foods: menu_selected
    },
    success: function (result) {
      window.location = "/result/food-i-cant-eat"
    }
  });
})