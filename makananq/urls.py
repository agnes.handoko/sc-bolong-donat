"""makananq URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app.views import *
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', homepage),
    path('food-i-cant-eat/', inputPage_foodCantBeEaten),
    path('food-i-want-eat/', inputPage_foodWantBeEaten),
    path('both-of-them/', inputPage_foodBothOfThem),
    path('result/food-i-can-eat/', resultPage_foodICanEat),
    path('result/food-i-cant-eat/', resultPage_foodICantEat),
    path('result/both-of-them/', resultPage_both),
    path('compute/food-i-can-eat/', compute_ICanEat),
    path('compute/food-i-cant-eat/', compute_ICantEat),
    path('compute/both/', compute_both)
]
