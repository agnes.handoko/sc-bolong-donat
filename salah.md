## Food I want to eat Salah
1. Bahan(x) -> InginMakan(x)
2. OlahanDari(y, x)
3. TerbuatDari(y, x)
4. InginMakan(x) -> [InginMakan(x) ^ Olahan(y)] v [InginMakan(x) ^ Menu(y)]
    //2. Bahan(x) ^ OlahanDari(y, x) -> Olahan(y)
5. InginMakan(x) ^ OlahanDari(y, x) -> MakanOlahan(y) P->Q
    //4. Bahan(x) ^ TerbuatDari(y, x) -> Menu(y)
6. InginMakan(x) ^ TerbuatDari(y, x) -> MakanMenu(y) R->S
    //6. [InginMakan(x) ^ Olahan(y)] v [InginMakan(x) ^ Menu(y)]
7. MakanOlahan(y) v MakanMenu(z) -> Makan(y)
8. Restriction(z) -> BisaMakan(Makan(y), ~(Makan(Mengandung(z))))

## Food I dont want to eat
1. Bahan(x) -> TidakInginMakan(x)
2. OlahanDari(y, x)
3. TerbuatDari(y, x)
4. TidakInginMakan(x) -> [TidakInginMakan(x) ^ Olahan(y)] v [TidakInginMakan(x) ^ Menu(y)]
5. TidakInginMakan(x) ^ OlahanDari(y, x) -> TidakMakanOlahan(y) P->Q
    //4. Bahan(x) ^ TerbuatDari(y, x) -> Menu(y)
6. TidakInginMakan(x) ^ TerbuatDari(y, x) -> TidakMakanMenu(y) R->S
    //6. [TidakInginMakan(x) ^ Olahan(y)] v [TidakInginMakan(x) ^ Menu(y)]
7. TidakMakanOlahan(y) v TidakMakanMenu(z) -> TidakMakan(y)
8. Restriction(z) -> BisaMakan(Makan(y), ~(Makan(Mengandung(z))))

## Salah
1. Bahan(x) -> TidakInginMakan(x)
2. TidakInginMakan(x) -> ~ ([InginMakan(x) ^ Olahan(y)] v [InginMakan(x) ^ Menu(y)])
2. InginMakan(x) -> [InginMakan(x) ^ Olahan(y)] v [InginMakan(x) ^ Menu(y)]
2. OlahanDari(y, x)
3. TerbuatDari(y, x)
4. InginMakan(x) ^ OlahanDari(y, x) -> MakanOlahan(y) P->Q
5. InginMakan(x) ^ TerbuatDari(y, x) -> MakanMenu(y) R->S
6. [InginMakan(x) ^ Olahan(y)] v [InginMakan(x) ^ Menu(y)]
7. MakanOlahan(y) v MakanMenu(z) -> Makan(y)
8. Restriction(z) -> BisaMakan(Makan(y), ~(Makan(Mengandung(z))))